<?php

/* @var $this yii\web\View */

use kartik\date\DatePicker;
use yii\helpers\Html;

$this->title = 'Тестовое задание для Tifia.com';

/* @var $params array */
/* @var $data array */
?>
<form method="post">
    <div class="form-group">
        <div class="row">
            <div class="col-md-3">
                <label>Id</label>
                <?php echo Html::input('text', 'referral[id]', $params['id'], ['class' => 'form-control', 'required' => true,]); ?>
            </div>
            <div class="col-sm-4 col-md-3">
                <label>Начало периода времени</label>
                <?php
                echo DatePicker::widget([
                    'name' => 'referral[start]',
                    'type' => DatePicker::TYPE_INPUT,
                    'value' => $params['start'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ],
                    'options' => [
                        'required' => true,
                    ]
                ]);
                ?>
            </div>
            <div class="col-sm-4 col-md-3">
                <label>Конец периода времени</label>
                <?php
                echo DatePicker::widget([
                    'name' => 'referral[end]',
                    'type' => DatePicker::TYPE_INPUT,
                    'value' => $params['end'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ],
                    'options' => [
                        'required' => true,
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Получить данные</button>
    </div>
</form>

<div class="form-group">
    <label>Суммарный объем:</label>
    <?php echo $data['sum_volume']; ?>
</div>
<div class="form-group">
    <label>Прибыльность:</label>
    <?php echo $data['profit']; ?>
</div>
<div class="form-group">
    <label>Количество прямых рефералов:</label>
    <?php echo $data['direct_ref_count']; ?>
</div>
<div class="form-group">
    <label>Количество всех рефералов:</label>
    <?php echo $data['all_ref_count']; ?>
</div>
<div class="form-group">
    <label>Количество уровней:</label>
    <?php echo $data['count_levels']; ?>
</div>
<div class="form-group">
    <label>Дерево рефералов:</label>
    <?php echo $data['tree']; ?>
</div>