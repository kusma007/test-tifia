<?php
namespace app\components;

use app\models\Accounts;
use app\models\Trades;
use app\models\Users;
use app\models\UsersTree;
use yii\helpers\ArrayHelper;

class Referrals
{
    /**
     *  Данные клиента по client_uid
     * @param $client_uid
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getUser($client_uid)
    {
        return Users::find()->where(['client_uid' => $client_uid])->one();
    }

    /**
     * Все рефералы
     * @param $user
     * @return mixed
     */
    public static function getDescendants($user)
    {
        return $user->descendants()->all();
    }

    /**
     * Прямые рефералы
     * @param $user
     * @return mixed
     */
    public static function getChildren($user)
    {
        return $user->children()->all();
    }

    /**
     * Споисок id всех рефералов
     * @param $descendants
     * @return array
     */
    public static function getDescendantsIdArr($descendants)
    {
        $return = [];
        foreach ($descendants as $item) {
            $return[] = $item['client_uid'];
        }
        return $return;
    }

    /**
     * Получение данных операций произведённых всеми рефералами клиента
     * @param $idArr
     * @param $fromTime
     * @param $toTime
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getTrades($idArr, $fromTime, $toTime)
    {
        return Trades::find()
            ->leftJoin(Accounts::tableName(), Accounts::tableName().'.login=' . Trades::tableName().'.login')
            ->where(['in', 'client_uid', $idArr])
            ->andWhere(['between', 'close_time', $fromTime . ' 00:00:00', $toTime . ' 23:59:59'])
            ->asArray()
            ->all();
    }

    /**
     * Подсчёт операций рефералов
     * @param $trades
     * @return array
     */
    public static function getTradesData($trades)
    {
        $return = [
            'sum_volume' => 0,
            'profit' => 0,
        ];
        foreach ($trades as $item) {
            $return['sum_volume'] += ($item['volume'] * $item['coeff_h'] * $item['coeff_cr']);
            $return['profit'] += $item['profit'];
        }

        return $return;
    }

    /**
     * Уровни вложенности рефералов
     * @param $id
     * @return mixed
     */
    public static function getCountLevels($id)
    {
        $treeData = UsersTree::find()
            ->select(['depth'])
            ->where(['parent' => $id])
            ->orderBy(['depth' => SORT_DESC])
            ->one();

        return ArrayHelper::getValue($treeData, ['depth'], 0);
    }

    /**
     * Дерево рефералов
     * @param $descendants
     * @param $user
     * @return string
     */
    public static function getTree($descendants, $user)
    {
        $tree = [];
        foreach ($descendants as $item) {
            $tree[] = [
                'client_uid' => $item['client_uid'],
                'partner_id' => $item['partner_id'],

            ];
        }

        $treeArr = self::MakeTree($tree,$user->client_uid);
        return self::getTreeList($treeArr);
    }

    /**
     * Построение списка дерева рефералов
     * @param $arr
     * @return string
     */
    public static function getTreeList($arr)
    {
        $list = '<ul>';
        foreach ($arr as $item) {
            $list .= '<li>' . $item['client_uid'];
            if (!empty($item['children'])) {
                $list .= self::getTreeList($item['children']);
            }
            $list .= '</li>';
        }
        $list .= '</ul>';
        return $list;
    }

    /**
     * Формирование массива для создания дерева рефералов
     * @param $arr
     * @param $client_uid
     * @return mixed
     */
    public static function MakeTree($arr, $client_uid){
        $parents_arr=array();
        foreach ($arr as $key => $value) {
            $parents_arr[$value['partner_id']][$value['client_uid']]=$value;
        }
        $tree=$parents_arr[$client_uid];
        self::createTree($tree, $parents_arr);
        return $tree;
    }

    /**
     * Формирование вложенных данных рефералов
     * @param $tree
     * @param $parents_arr
     */
    public static function createTree(&$tree, $parents_arr){
        foreach ($tree as $key => $value) {
            if(!isset($value['children'])) {
                $tree[$key]['children']=array();
            }
            if(array_key_exists($key, $parents_arr)){
                $tree[$key]['children']=$parents_arr[$key];
                self::createTree($tree[$key]['children'], $parents_arr);
            }
        }
    }


    /**
     * Получение реферальных данных клиента
     * @param $client_uid
     * @param $fromTime
     * @param $toTime
     * @return array
     */
    public static function getUserReferralData($client_uid, $fromTime, $toTime)
    {
        $user = self::getUser($client_uid);
        $descendants = self::getDescendants($user);
        $tree = self::getTree($descendants, $user);
        $children = self::getChildren($user);
        $idArr = self::getDescendantsIdArr($descendants);
        $trades = self::getTrades($idArr, $fromTime, $toTime);
        $tradesData = self::getTradesData($trades);
        $countLevels = self::getCountLevels($user->id);

        $return = [
            'sum_volume' => $tradesData['sum_volume'],
            'profit' => $tradesData['profit'],
            'direct_ref_count' => count($children),
            'all_ref_count' => count($descendants),
            'count_levels' => $countLevels,
            'tree' => $tree,
        ];

        return $return;
    }
}