<?php
namespace app\models;

use valentinek\behaviors\ClosureTableQuery;
use yii\db\ActiveQuery;

class UsersQuery extends ActiveQuery
{
    public function behaviors() {
        return [
            [
                'class' => ClosureTableQuery::class,
                'tableName' => 'users_tree'
            ],
        ];
    }
}