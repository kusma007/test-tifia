<?php

namespace app\controllers;

use app\models\Accounts;
use app\models\Trades;
use app\models\User;
use app\models\Users;
use app\models\UsersTree;
use app\components\Referrals;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $params = Yii::$app->getRequest()->post('referral', []);

        // Дефолтные значения параметров
        if(empty($params)) {
            $start = date('Y-m-d', strtotime('-1 month'));
            $end = date('Y-m-d');
            $params = [
                'start' => $start,
                'end' => $end,
                'id' => 82824897,
            ];
        }

        $data = Referrals::getUserReferralData($params['id'], $params['start'], $params['end']);
        return $this->render('index', [
            'data' => $data,
            'params' => $params,
        ]);
    }

}
