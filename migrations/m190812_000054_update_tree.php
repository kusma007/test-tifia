<?php

use app\models\Users;
use app\models\UsersTree;
use yii\db\Migration;

/**
 * Class m190812_000054_update_tree
 */
class m190812_000054_update_tree extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createIndex('idx-users-client_uid','users', 'client_uid');
        $this->createIndex('idx-users-partner_id', 'users', 'partner_id');
        $this->createIndex('idx-trades-login', 'trades', 'login');
        $this->createIndex('idx-accounts-client_uid', 'accounts', 'client_uid');

        for ($i = 0; $i <= 4; $i++) {
            if($i == 0) {
                $rootUsers = Users::find()->where(['partner_id' => 0])->all();
                foreach ($rootUsers as $item) {
                    if($item->id != 90) {
                    $item->saveNodeAsRoot(false);
                    }
                }
            } else {
                $this->insertChildren($i);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->truncateTable('users_tree');
        $this->dropIndex('idx-accounts-client_uid','accounts');
        $this->dropIndex('idx-trades-login','trades');
        $this->dropIndex('idx-users-partner_id','users');
        $this->dropIndex('idx-users-client_uid','users');
    }

    function insertChildren($level)
    {
        if($level > 0) {
            $level--;
        }
        $data = UsersTree::find()->with(['child0', 'child0.children'])->where(['depth' => $level])->all();

        foreach ($data as $item) {
            foreach ($item->child0->children as $child) {
                $item->child0->append($child);
            }
        }
    }

}
